import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardUser implements CanActivate {


  constructor( private router: Router) {}
  // tslint:disable-next-line:typedef
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('trainerName') === null || localStorage.getItem('trainerName') === '' ){
      // @ts-ignore
      return true;
    }else{
      this.router.navigateByUrl('/catalogue');
      return false;
    }
  }
}
