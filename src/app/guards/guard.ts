import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor( private router: Router) {}
  // tslint:disable-next-line:typedef
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('trainerName') === null || localStorage.getItem('trainerName') === '' ){
      // @ts-ignore
      this.router.navigateByUrl('/login');
      return false;
    }else{
      return true;
    }
  }
}
