import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ProfilePage } from './pages/profile.page';
import {ProfileRoutingModules} from "./profile-routing.modules";

@NgModule({
    declarations: [ProfilePage],
    imports: [
        ProfileRoutingModules,
        CommonModule,
        FormsModule,
        // CommonModule
    ]
})

export class ProfileModule {}