import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfilePage {


  constructor(private router: Router) {
  }


  public trainerName = localStorage.getItem("trainerName");
  public collectedPokemon = localStorage.getItem("collectedPokemon");
  public ownPokemon = JSON.parse(localStorage.getItem('collectedPokemon'));

  backToCatalogue() {
    this.router.navigate(['/catalogue']);
  }

  logOut() {
    window.localStorage.clear();
    this.router.navigate(["/login"])
  }

}

