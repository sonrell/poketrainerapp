import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {LoginPage} from './login/pages/login-page/login.page';
import {CataloguePage} from './catalogue/pages/catalogue.page';
import {DetailPage} from './detailpage/pages/detailpage.page';
import {ProfilePage} from './profile/pages/profile.page';

@NgModule({
  declarations: [
    AppComponent,
    // LoginPage,
    CataloguePage,
    DetailPage,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
