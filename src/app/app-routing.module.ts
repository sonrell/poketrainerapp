import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {LoginModule} from './login/login.module';
import {AuthGuard} from './guards/guard';
import {AuthGuardUser} from './guards/guard-user';



const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/catalogue'

  },
  {
    path: 'login',
    canActivate: [AuthGuardUser],
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'catalogue',
    canActivate: [AuthGuard],
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule)
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'detail',
    canActivate: [AuthGuard],
    loadChildren: () => import("./detailpage/detailpage.module").then(m => m.DetailModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
