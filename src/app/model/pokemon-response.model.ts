import {Pokemon} from './pokemon.model';


export interface PokemonResp{
  count: number;
  next: string;
  prev: string;
  results: Pokemon[];
}
