export interface Pokemon{
  id?: string;
  name: string;
  url: string;
  image?: string;
  weight?: number;
  height?: number;
  types?: Type[];
  stats?: PokemonStat[];
  abilities?: Abilities[];
  base_experience: number;
  moves: Moves[];
}

export interface PokemonType{
  slot: number;
  type: PokemonTypeType;
}

export interface PokemonTypeType{
  name: string;
  url: string;
}

export interface PokemonStat{
  base_stat: number;
  effort: number;
  stat: PokemonStatStat;
}

export interface PokemonStatStat{
  name: string;
  url: string;
}
export interface Abilities{
  ability: AbilitiesAbs;
  is_hidden: boolean;
  slot: number;
}

export interface AbilitiesAbs{
  name: string;
  url: string;
}
export interface Moves{
  move: MovesMoves;
  version_group_details: any[];
}
export interface MovesMoves{
  name: string;
  url: string;
}

export interface Type{
  slot: number;
  type: TypeType;
}

export interface TypeType{
  name: string;
  url: string;
}
