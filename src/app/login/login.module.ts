import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import {LoginPage} from './pages/login-page/login.page';

@NgModule({
    declarations: [LoginPage],
    imports: [
        FormsModule,
        LoginRoutingModule,
        CommonModule,
    ]
})

export class LoginModule {}
