import {Component} from "@angular/core"
import { Router } from "@angular/router";


@Component({
    selector: "app-login-page",
    templateUrl: "./login.page.html",
    styleUrls: ["./login.page.css"]
})


export class LoginPage {


    constructor(private router: Router) { }
    public trainerName: string ="";

  // tslint:disable-next-line:typedef
    onKey(trainerName: string){
        this.trainerName = trainerName;

        console.log(trainerName);
        localStorage.setItem('trainerName', trainerName);
        this.router.navigate(["/catalogue"]);
        }
}


