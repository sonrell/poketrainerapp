import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import { Pokemon } from 'src/app/model/pokemon.model';
import {PokemonResp} from '../../model/pokemon-response.model';

// @ts-ignore
const {pokeApi} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  public pokemon: Pokemon[] = [];
  constructor(private readonly http: HttpClient ) {
  }

  fetchPoke(): Observable<any> {
    // console.log(this.http.get(`${pokeApi}pokemon`));
    return this.http.get(`${pokeApi}pokemon?limit=151`)
      .pipe(
        map((response: PokemonResp) => {
          return response.results.map(pokemon => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url),
            ...this.getAbilities(pokemon.url),
          }));
        }));
  }


  private getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
  private getAbilities(url: string): any{
    return {ab: this.http.get(url).pipe(
        map((response: any) => response.abiilities.map(ab => ab.name))
    )};
  }

}
