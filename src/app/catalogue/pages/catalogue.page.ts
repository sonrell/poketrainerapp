import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import {PokemonService} from './catalogue.facade';


@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CataloguePage implements OnInit {
  pokemon: any[] = [];
  error = '';

  public pokeName: string = "";


  constructor(private readonly  PokeS: PokemonService, private router: Router) {
  }

  public trainerName = localStorage.getItem('trainerName');

  ngOnInit(): void {
    this.PokeS.fetchPoke().subscribe(pokemon => {
        this.pokemon = pokemon;
      },
      error1 => {
        this.error = error1.message;
      });
    console.log(this.pokemon);
  }

  // tslint:disable-next-line:typedef
  btnClick() {
    this.router.navigate(['/profile']);
  }

  choosePokemon(name,image,weight) {
    localStorage.setItem('chosenPokeMon', name);
    localStorage.setItem("pokeImage",image)
    localStorage.setItem("pokeWeight", weight)
    this.router.navigate(['/detail']);
  }

}





