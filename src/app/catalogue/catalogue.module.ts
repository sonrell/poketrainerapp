import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {CatalogueRoutingModules} from './catalogue-routing.modules';

@NgModule({
    declarations: [],
    imports: [
        CatalogueRoutingModules,
        CommonModule,
        FormsModule
    ]
})

export class CatalogueModule {}
