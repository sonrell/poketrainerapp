import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {DetailRoutingModules} from "./detailpage-routing.modules";

@NgModule({
    declarations: [],
    imports: [
        DetailRoutingModules,
        CommonModule,
        FormsModule
    ]
})

export class DetailModule {}