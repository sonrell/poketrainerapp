import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {PokemonDetailService} from '../../pokemon-detail/pokemon-detail.service';
import {Pokemon} from '../../model/pokemon.model';


@Component({
  selector: 'app-detailpage',
  templateUrl: './detailpage.component.html',
  styleUrls: ['./detailpage.component.css']
})
export class DetailPage implements OnInit{
  private readonly pokeName: string = '';

  // tslint:disable-next-line:no-shadowed-variable
  constructor(private router: Router, private readonly pokeDesSer: PokemonDetailService) {
    this.pokeName = localStorage.getItem('chosenPokeMon');
  }

  public chosenPokeMon = localStorage.getItem('chosenPokeMon');
  public chosenPokeMonImage = localStorage.getItem('pokeImage');
  public collectedPokemon = JSON.parse(localStorage.getItem('collectedPokemon'));
  public currentPokemon = {
    name: this.chosenPokeMon,
    image: this.chosenPokeMonImage
  };

  // tslint:disable-next-line:typedef
  btnClick() {
    this.router.navigate(['/catalogue']);
}

  ngOnInit(): void {
    this.pokeDesSer.fetchPokeByName(this.pokeName);
  }

  collect = () => {
    if (this.collectedPokemon === null){
      // this.collectedPokemon.push(this.currentPokemon)
      this.collectedPokemon = [];
      this.collectedPokemon.push(this.currentPokemon);
      localStorage.setItem('collectedPokemon', JSON.stringify(this.collectedPokemon));
      alert('1.2.3....... COLLECTED');
      console.log('hei');
    }else{

      if (!this.collectedPokemon.some(pokemon => pokemon.name === this.currentPokemon.name)) {
        this.collectedPokemon.push(this.currentPokemon);
        console.log(this.collectedPokemon);
        localStorage.setItem('collectedPokemon', JSON.stringify(this.collectedPokemon));
        console.log('pushed');
        alert('1.2.3....... COLLECTED');
        this.router.navigate(['/catalogue']);
      }else{
       console.log('not pushed');
       alert('You have already collected this pokemon');
      }
    }

  }


  get pokemon(): Pokemon {
    return this.pokeDesSer.pokemon;
  }


}

