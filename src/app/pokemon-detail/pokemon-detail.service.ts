import{Injectable} from '@angular/core';
import {Pokemon} from '../model/pokemon.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';


const {pokeApi} = environment ;
@Injectable({
  providedIn: 'root'
})
export class PokemonDetailService{
  public pokemon: Pokemon;
  constructor(private readonly  http: HttpClient) {
  }
  public fetchPokeByName(name: string): void{
    // @ts-ignore
    return this.http.get<Pokemon>(`${pokeApi}pokemon/${name}`)
      .subscribe((pokemon: Pokemon) => {
        this.pokemon = pokemon;
      });
  }
}
